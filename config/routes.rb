Rails.application.routes.draw do

  get 'access/menu'
  get 'access/login'
  post 'access/attempt_login'
  get 'access/logout'

  resources :subjects do
    member do
      get :delete
    end

    # for collection actions:
    # collection do
    #   get :export
    # end

  end
  resources :pages do
    member do
      get :delete
    end
  end

  resources :users do
    member do
      get :delete
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
