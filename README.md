#Simple CMS#
##Overview##
This is my log as I re-follow the Ruby on Rails course on LinkedIn Learning. During the COVID-19 shutdown, I am currently laid off work, and planning to use the time and energy to start building the projects I've dreamed of for a long time. My intention is to get my coding legs back under me by following an updated version of a course I've taken before, but have not retained the knowledge from. I will spend some time building out that sample project, using the knowledge I'm gaining to optimize and document this sample project in ways that the original tutorial did not, to use as a resumé piece. Examples of alterations I plan to make:

- Using learning from  an HTML course to improve the HTML semantics of the CMS built in the Rails course.
- Using learning from a CSS course to build the style for the CMS, rather than copy/pasting the default CSS from the provided course files
- Building test code for the CMS

I will use this document as a log of my progress and challenges, and to practice external documentation.

##3/31/2020##

Today starts the first day of re-building the Simple CMS project, starting from a blank project. In watching the course videos, I still find Routing a little elusive. It seems like excessive code is needed for default actions.
##4/1/2020##

Finishing the Rails course today. Resourceful routes are covered near the end of the course, and it makes much more sense (though, I'm still not sure why the controller generator also still creates static routes for actions given as arguments, when resourceful routes are the default).

As I near completion, I see that the current version of the Rails course does not provide a copy/paste CSS like previous versions have. Which works out just as well for me in this context, since I'll have a blank slate to start from in the CSS course.

##4/2/2020##

Ended up saving the last couple of lessons in the Rails 6 course today. Got those finished, I'm going to go through some spots in the older Rails 5 course, as it was much more complete (Rails 6 doesn't include User authentication, templates, asset pipeline, cookies & sessions, etc.